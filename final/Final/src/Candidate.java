import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import org.vu.contest.ContestEvaluation;
//@SuppressWarnings("unused")
public class Candidate {
	public double[] genotype;
	public double fitness;
	public int age;
	public float linRankProb;
	public ArrayList<Double> sigmaList;
	public float sigmaScale;
	public float sigmaScaleProb;
	public Random rnd_;
	
	public Candidate(Random rnd_){
		
		genotype = new double[10];
//		for(int i = 0; i < 10; i++){
//			double randomValue = getRandomDouble(ParameterSettings.SEARCH_SPACE_MIN,ParameterSettings.SEARCH_SPACE_MAX);
//			genotype[i] = (randomValue);
//					
//		}
		fitness = Double.NaN;
		age = 0;
		linRankProb = (float) -1.0;
		sigmaScale = (float) -1.0;
		sigmaScaleProb = (float) 0.0;
		sigmaList = new ArrayList<Double>();
		this.rnd_ = rnd_;
		for(int i = 0; i <10; i++){
			sigmaList.add(ParameterSettings.INITIAL_SIGMA);
		}
	}
	
	public Candidate(boolean init, Random rnd_){
		genotype = new double[10];
		this.rnd_ = rnd_;
		for(int i = 0; i < 10; i++){
			double randomValue = getRandomDouble(ParameterSettings.SEARCH_SPACE_MIN,ParameterSettings.SEARCH_SPACE_MAX);
			genotype[i] = (randomValue);
					
		}
		fitness = Double.NaN;
		age = 0;
		linRankProb = (float) -1.0;
		sigmaScale = (float) -1.0;
		sigmaScaleProb = (float) 0.0;
		sigmaList = new ArrayList<Double>();
	
//		for(int i = 0; i <10; i++){
//			sigmaList.add(ParameterSettings.INITIAL_SIGMA);
//		}

	}
//
//	public void setFitness(ContestEvaluation evaluation){
//	
//		this.fitness = (double) evaluation.evaluate(genotype);
//		
//
//	}
	
	public Candidate(double[] genotype, double fitness, int age){
		this.genotype = genotype;
		this.fitness = fitness;	
		this.age = age;
	}
	
	
	public double getRandomDouble(int min, int max){
		double randomValue = min + (max- min) * rnd_.nextDouble();
		return randomValue;
	}
	
	public static Comparator<Candidate> parSorFit = new Comparator<Candidate>(){
		public int compare(Candidate i1, Candidate i2){
			double fit1 = i1.fitness;
			double fit2 = i2.fitness;
			
			//return (int) (fit2-fit1);
			if(fit1<fit2){
				return 1;
			}
			else if(fit1 > fit2){
				return -1;
			}
			else{
				return 0;
			}
		
	}};
}
