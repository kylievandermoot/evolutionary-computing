import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.util.Random;
import java.io.PrintStream;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@SuppressWarnings("unused")
public class player5 implements ContestSubmission
{
	public class run {

	}

	boolean isMultimodal;
	boolean hasStructure; 
	boolean isSeparable;
	Random rnd_;
	ContestEvaluation evaluation_;
	private int evaluations_limit_;
	Population population;
	ParameterSettings para;
//	ArrayList<Integer> indexes;
//	ArrayList<Integer> indexesAll;

//	ArrayList<Integer> genoIndex;

	public player5()
	{
		rnd_ = new Random();
		population = new Population(rnd_);
		para = new ParameterSettings();
//		multimodel = false;
//		seperable = false;
//		regular = false;
//		indexes = new ArrayList<Integer>();
//		indexesAll = new ArrayList<Integer>();
//		genoIndex = new ArrayList<Integer>();

	}

//	public void createGenoIndex(){
//		for(int i = 0; i < 10; i++){
//			genoIndex.add(i);
//		}
//	}
//
//	public void createIndexAll(){
//		for(int i = 0; i < ParameterSettings.POPULATION_SIZE + ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION; i++){
//			indexesAll.add(i);
//		}
//	}
//
//	public void createIndex(){
//
//		for(int i = 0; i < ParameterSettings.POPULATION_SIZE; i++){
//			indexes.add(i);
//		}
//
//	}
//
//	public void shuffleIndexAll(){
//		Collections.shuffle(indexesAll);
//	}
//
//	public void shuffleIndex(){
//		Collections.shuffle(indexes);
//	}
//
//	public void shuffleGenoIndex(){
//		Collections.shuffle(genoIndex);
//	}
//
	public void setSeed(long seed)
	{
		// Set seed of algortihms random process
		rnd_.setSeed(seed);
	}

	public void setEvaluation(ContestEvaluation evaluation)
	{
		// Set evaluation problem used in the run
		evaluation_ = evaluation;

		// Get evaluation properties
		Properties props = evaluation.getProperties();
		// Get evaluation limit
		evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
		// Property keys depend on specific evaluation
		// E.g. double param = Double.parseDouble(props.getProperty("property_name"));
		isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
		hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
		isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

		// Do sth with property values, e.g. specify relevant settings of your algorithm
//		if(isMultimodal){
//			multimodel = true;
//
//		}else{
//			// Do sth else
//		}
//		
//		if(isSeparable){
//			seperable = true;
//		}
	}


	public void printPopulation(Population population){
		for(int i = 0; i < population.list.size(); i++){

			Candidate individual = population.list.get(i);
			System.out.print("Parent: ");
			System.out.print(i);
			System.out.print("\n");
			System.out.print("fitness: ");
			System.out.print(individual.fitness);
			System.out.print("\n");
			System.out.print("Genotype: ");
			for(int j = 0; j < 10; j++){
				System.out.print(individual.genotype[j]);
				System.out.print(", ");
			}
			System.out.print("\n");
		}}

	public boolean getRandomBoolean(double d){
		return rnd_.nextDouble()<d;
	}

	public Population selectParentsLinRank(Population parentPopulation){
		Population result = new Population(rnd_);

		//parentPopulation.sortParentPopulation();
		// NOTE floris: hier worden alle RankingProbs berekend, maar meestal zal een van de top-rated
		// Candidates worden geselecteerd. Misschien wordt deze parent selection dus sneller als het
		// volgende idee wordt gebruikt
		//   0. pak Candidate met beste fitness, op index N=0
		//   1. bereken de RankingProb voor deze Candidate en voeg toe aan sum
		//   2. vergelijk sum met  getal en
		//      a. stop als sum groter
		//      b. pak Candidate met volgende-beste fitness op index N+1 en ga naar (1)
		// Stel dat de parents gemiddeld binnen de top-10 van elke Populatie worden genomen, dan
		// kan deze 'optimalisatie' deze functie ongeveer 10x zo snel maken.
		//parentPopulation.setLinearRankingProb();
		
		

		//parentPopulation.setRandomOrder();

		int current_member = 1;
		while(current_member <= ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1){
			//double r = rnd.nextDouble();
			double r = rnd_.nextDouble();
			int i = 0;
			double sum = 0;

			while(sum < r && i < parentPopulation.list.size()){
				Candidate c = parentPopulation.list.get(i);
				int rank = parentPopulation.list.size()-1-i;
				float constant = (float) ((2-ParameterSettings.SELECTIOIN_PRESURE)/parentPopulation.list.size());
				float variable = (float) (((2*rank)*(ParameterSettings.SELECTIOIN_PRESURE-1))/(parentPopulation.list.size()*(parentPopulation.list.size()-1)));
				c.linRankProb = constant + variable;
				sum += parentPopulation.list.get(i).linRankProb;
				i++;
			}	

			result.list.add(parentPopulation.list.get(i-1));
			current_member += 1;
		}


		//result = parentPopulation.getTopIParents(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION);

		return result;		
	}

	public Population selectParentsTournement(Population population){
		Population result = new Population(rnd_);
		Population tournement = new Population(rnd_);
		for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1; i++){
			tournement.list.clear();
			//shuffleIndex();
			//System.out.println(indexes.get(0));
			
			for(int j = 0; j < ParameterSettings.TOURNEMENT_SIZE; j++){
				int index = rnd_.nextInt(population.list.size());
				tournement.list.add(population.list.get(index));

			}
			tournement.sortParentPopulation();

			result.list.add(tournement.getTopIParents(1).list.get(0));
			
		}

		return result;
	}

	
	//Parent list was not sorted 
	public Population selectParentsSigmaScaling(Population parentPopulation, double sigmaScale){
		Population result = new Population(rnd_);

		double sumSigmaScale = parentPopulation.setSigmaScale(sigmaScale);

		int current_member = 1;
		while(current_member <= ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION2){
			double r = rnd_.nextDouble();
			int i = 0;
			double sum = 0;
			
			//parentPopulation.sortParentPopulation();
			
			while(sum < r && i < parentPopulation.list.size()){
					Candidate c = parentPopulation.list.get(i);
					c.sigmaScaleProb = (float) (c.sigmaScale/sumSigmaScale);
					sum += c.sigmaScaleProb;
					i++;	
			}	

			result.list.add(parentPopulation.list.get(i-1));
			current_member += 1;
		}

		return result;
	}
	
	public Population selectSurvivorsSigmaScaling(Population parentPopulation, double sigmaScale){
		Population result = new Population(rnd_);

		double sumSigmaScale = parentPopulation.setSigmaScale(sigmaScale);

		int current_member = 1;
		while(current_member <= ParameterSettings.POPULATION_SIZE){
			double r = rnd_.nextDouble();
			int i = 0;
			double sum = 0;
			
			//parentPopulation.sortParentPopulation();
			
			while(sum < r && i < parentPopulation.list.size()){
					Candidate c = parentPopulation.list.get(i);
					c.sigmaScaleProb = (float) (c.sigmaScale/sumSigmaScale);
					sum += c.sigmaScaleProb;
					i++;	
			}	

			result.list.add(parentPopulation.list.get(i-1));
			current_member += 1;
		}

		return result;
	}

	public Population applyCrossOver(Population parents){
		if(isMultimodal == false){
			Population result = new Population(rnd_);

			
			for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1/2; i++){
				Candidate c1 = new Candidate(rnd_);
				Candidate c2 = new Candidate(rnd_);
								
				Candidate p1 = parents.list.get(i);
				Candidate p2 = parents.list.get(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1-1-i);

				int gene = rnd_.nextInt(10);

				for(int j = 0; j < 10; j++){
					if(j != gene){
						c1.genotype[j] = p1.genotype[j];
						c2.genotype[j] = p2.genotype[j];
					}else{
						c1.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
						c2.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
					}
				}

				result.list.add(c1);
				result.list.add(c2);
			}

			return result;
		}else{
			Population result = new Population(rnd_);
			
			
			for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION2/2; i++){
				Candidate p1 = parents.list.get(i*2);
				Candidate p2 = parents.list.get(i*2+1);
				//				Candidate p2 = parents.list.get(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION-1-i);

				Candidate c1 = new Candidate(rnd_);

				for(int j = 0; j < 10;j++){
					c1.genotype[j]=(p1.genotype[j]+p2.genotype[j])/2;	
				}

				result.list.add(c1);
				result.list.add(c1);


			}
			return result;
		}

	}

	
	//Er werd nog geen sigmalist van de parents meegegeven aan de kinderen, deze was altijd nog de intitial sigma
	public Population applySingleArithmeticFunction2(Population parents){
		Population result = new Population(rnd_);
		
		for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION2/2; i++){
			
			Candidate p1 = parents.list.get(i);
			Candidate p2 = parents.list.get(rnd_.nextInt(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION2));

			Candidate c1 = new Candidate(rnd_);
			Candidate c2 = new Candidate(rnd_);

			int gene = rnd_.nextInt(10);

			for(int j = 0; j < 10; j++){
				if(j != gene){
					c1.genotype[j] = p1.genotype[j];
					c2.genotype[j] = p2.genotype[j];
					c1.sigmaList.add(j, p1.sigmaList.get(j));
					c1.sigmaList.add(j, p2.sigmaList.get(j));
				}else{
					c1.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
					c2.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
					c1.sigmaList.add(j, (p1.sigmaList.get(j) + p2.sigmaList.get(j)) / 2);
					c2.sigmaList.add(j, (p1.sigmaList.get(j) + p2.sigmaList.get(j)) / 2);
				}
			}
			//c1.setFitness(evaluation_);
			//c2.setFitness(evaluation_);
			
			result.list.add(c1);
			result.list.add(c2);
		}

		return result;
	}

	
	
	//Er werd nog geen sigmalist van de parents meegegeven aan de kinderen, deze was altijd nog de intitial sigma
	public Population applyWholeArithmeticFunction2(Population parents){
		Population result = new Population(rnd_);
		
		for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION2/2; i++){
			
			Candidate p1 = parents.list.get(i);
			Candidate p2 = parents.list.get(rnd_.nextInt(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION2));

			Candidate c1 = new Candidate(rnd_);
			Candidate c2 = new Candidate(rnd_);


			for(int j = 0; j < 10; j++){
				c1.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
				c2.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
				c1.sigmaList.add(j, (p1.sigmaList.get(j) + p2.sigmaList.get(j)) / 2);
				c2.sigmaList.add(j, (p1.sigmaList.get(j) + p2.sigmaList.get(j)) / 2);

			}
			//c1.setFitness(evaluation_);
			//c2.setFitness(evaluation_);
			
			result.list.add(c1);
			result.list.add(c2);
		}

		return result;
	}
	
	
	
	public Population applyCrossOverAverage(Population parents){
		Population result = new Population(rnd_);

		for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1/2; i++){
			Candidate p1 = parents.list.get(i);
			Candidate p2 = parents.list.get(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1-1-i);

			Candidate c1 = new Candidate(rnd_);
			Candidate c2 = new Candidate(rnd_);

			int index = rnd_.nextInt(10);

			for(int j = 0; j< index;j++){
				c1.genotype[j] = p1.genotype[j];
				c2.genotype[j] = p2.genotype[j];
			}
			for(int j = index; j< 10; j++){
				c1.genotype[j] =(p1.genotype[j]+p2.genotype[j])/2;
				c2.genotype[j] = (p1.genotype[j]+p2.genotype[j])/2;
			}
			result.list.add(c1);
			result.list.add(c2);
		}

		return result;
	}

	public Population applyNewCrossOver(Population parents){
		Population result = new Population(rnd_);

		for(int i = 0; i < ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1/2; i++){
			Candidate p1 = parents.list.get(i);
			Candidate p2 = parents.list.get(ParameterSettings.NUMBER_OF_PARENTS_IN_SELECTION1-1-i);

			Candidate c1 = new Candidate(rnd_);
			Candidate c2 = new Candidate(rnd_);

			int index = rnd_.nextInt(10);

			for(int j = 0; j< index;j++){
				c1.genotype[j] = p1.genotype[j];
				c2.genotype[j] = p2.genotype[j];
			}
			for(int j = index; j< 10; j++){
				c1.genotype[j] = p2.genotype[j];
				c2.genotype[j] = p1.genotype[j];
			}
			result.list.add(c1);
			result.list.add(c2);
		}

		return result;
	}


	// TODO waarschijnlijk werkt selectBestSurvivors nog beter op functie 1 omdat deze zsm naar het optimum moet 
	public Population selectSurvivorsFunction1(Population all){
		Population result = new Population(rnd_);

		all.sortParentPopulation();
		all.setLinearRankingProb(ParameterSettings.SELECTIOIN_PRESURE);
	
		all.setRandomOrder();

		int current_member = 1;
		while(current_member <= ParameterSettings.POPULATION_SIZE ){
			double r = rnd_.nextDouble();

			int i = 0;
			double sum = 0;
			while(sum < r && i < all.list.size()){
				sum += all.list.get(i).linRankProb;
				i++;
			}	

			result.list.add(all.list.get(i-1));
			current_member += 1;
		}

		//		all.sortParentPopulation();
		//		result = all.getTopIParents(ParameterSettings.POPULATION_SIZE);

		return result;
	}
	
	
	public Population selectSurvivors(Population all){
		Population result = new Population(rnd_);

		all.sortParentPopulation();
		all.setLinearRankingProb(ParameterSettings.SELECTION_PRESURE2);

		//all.setRandomOrder();

		int current_member = 1;
//		System.out.println("NEW");
		while(current_member <= ParameterSettings.POPULATION_SIZE ){
			double r1 = rnd_.nextDouble();
			double r2 = rnd_.nextDouble();
			double r = r1 * r2;
			int i = 0;
			double sum = 0;
			//System.out.println("Random");
			///System.out.println(r);
			
			while(sum < r && i < all.list.size()){
				
				sum += all.list.get(i).linRankProb;
				//System.out.println(sum);
				i++;
			}	
//			System.out.println(i-1);
			result.list.add(all.list.get(i-1));
			current_member += 1;
		}

		//		all.sortParentPopulation();
		//		result = all.getTopIParents(ParameterSettings.POPULATION_SIZE);
//		System.out.println(result.list.size());
		return result;
	}
	
	

	public Population selectBestSurvivors(Population all){
		Population result = new Population(rnd_);
		all.sortParentPopulation();
		result.list.addAll(all.getTopIParents(ParameterSettings.POPULATION_SIZE).list);

		return result;
	}

	
	public Population selectWithoutParents(Population childs){
		Population result = new Population(rnd_);
		
		
		return result;
	}
	
	
	public void run(){
		//createIndex();
		//createIndexAll();
		//createGenoIndex();
		//System.out.println("hoi ik zit in run");
		if(!isMultimodal){//function 1

	
			int evals = 0;


			population.initPopulation();

			population.setFitness(evaluation_);

			double mutationRate = 1;

			int count = 1;
			while(evals < evaluations_limit_-population.list.size()){
				Population selectedParents = selectParentsLinRank(population);

				Population childs = applyNewCrossOver(selectedParents);

				childs.applyMutationFunction1((mutationRate-0.99*((double)evals/evaluations_limit_)));
				childs.setFitness(evaluation_);

				Population all = new Population(population.list, rnd_);
				all.merge(childs);

				population = selectBestSurvivors(all);

				evals += childs.list.size();


			}
		}else{

				int evals = 0;
				population.initPopulation();
				population.setFitness(evaluation_);
				
				ArrayList<Double> initSigma = new ArrayList<Double>(Arrays.asList(ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA,ParameterSettings.INITIAL_SIGMA));
				
				population.initSigma(initSigma);
				
				while(evals < evaluations_limit_ - population.list.size()){
					
					//kiest een aantal parents 
					Population selectedParents = selectParentsSigmaScaling(population, ParameterSettings.SIGMA_SCALING);
					
//					Population childs = applyWholeArithmeticFunction2(selectedParents);

					Population childs = applySingleArithmeticFunction2(selectedParents);
					
					//childs.applyMutation2(ParameterSettings.ALPHA);
					childs.applyMutationFunction1((ParameterSettings.MUTATION_RATE2-0.99*((double)evals/evaluations_limit_)));
					childs.setFitness(evaluation_);
					
					Population all = new Population(population.list, rnd_);

//					for(int i = 0; i < selectedParents.list.size(); i++){
//						int index = all.list.indexOf(selectedParents.list.get(i));
//						if(index != -1){
//							all.list.remove(index);
//						}
//					}	
////					System.out.println(all.list.size());
//					
//					selectedParents.merge(childs);
//					
//					selectedParents.sortParentPopulation();
//					Population survivors = selectedParents.getTopIParents(ParameterSettings.POPULATION_SIZE - all.list.size());
//					all.merge(survivors);
					
					//Dit was het eerst
					all.merge(childs);
					//population = selectBestSurvivors(all);
//					
					population = selectSurvivors(all);
					evals += childs.list.size();


				}

			}

		}



	public static void main(String[] args) {

	}


}